import Hero from '../components/Hero';
import { Layout } from '../layout/Layout';
import styles from '../styles/Home.module.css';
import { Section } from '../styles/GlobalComponents/GlobalStyles';
import BgAnimation from '../components/BackgroundAnimation';
import Projects from '../components/Projects';
import Technologies from '../components/Technologies';
import TimeLine from '../components/TimeLine';
import Accomplishments from '../components/Accomplishments';

export default function Home() {
  return (
    <Layout>
      <Section grid>
        <Hero />
        <BgAnimation />
      </Section>
      <Projects />
      <Technologies />
      <TimeLine />
      <Accomplishments />
    </Layout>
  );
}
