import React from 'react';

import { ButtonBack, ButtonFront } from './GlobalStyles';

interface ButtonType {
  alt?: string;
  form?: string;
  disabled?: boolean;
  onClick?: () => {};
  children: React.ReactNode;
}

const Button = (props: ButtonType) => (
  <ButtonBack alt={props.alt} form={props.form} disabled={props.disabled}>
    {props.children}
    <ButtonFront
      // alt={props.alt}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </ButtonFront>
  </ButtonBack>
);

export default Button;
