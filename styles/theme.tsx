import { ThemeProvider } from 'styled-components';

import theme from '../themes/default';
import GlobalStyles from './globals';

interface ThemeType {
  children: React.ReactNode;
}

const Theme = ({ children }: ThemeType) => (
  <ThemeProvider theme={theme}>
    <GlobalStyles />
    {children}
  </ThemeProvider>
);

export default Theme;
